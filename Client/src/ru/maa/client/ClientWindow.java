package ru.maa.client;

import ru.maa.network.TCPConnection;
import ru.maa.network.TCPConnectionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Anton on 15.02.2018.
 */
public class ClientWindow extends JFrame implements ActionListener, TCPConnectionListener{

    private static final String IP_ADDR = "192.168.43.230";
    public static final int PORT = 8189;
    public static final int WIDTH = 600;
    public static final int HEIGHT = 400;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {new ClientWindow();}
        });
    }

    private final JTextField fieldNickname = new JTextField("Гость");
    private final JTextArea log = new JTextArea();
    private final JTextField fieldInput = new JTextField();

    private TCPConnection connection;

    private ClientWindow() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WIDTH,HEIGHT);
        setLocationRelativeTo(null);
        setAlwaysOnTop(true);

        add(fieldNickname, BorderLayout.NORTH);

        log.setEditable(false);
        log.setLineWrap(true);
        add(log , BorderLayout.CENTER);

        fieldInput.addActionListener(this);
        add(fieldInput,BorderLayout.SOUTH);

        setVisible(true);
        try {
            connection = new TCPConnection(this,IP_ADDR,PORT);
        }catch (IOException e) {
            printMessage("Connection exception: " + e);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String msg = fieldInput.getText();
        if(msg.equals(""))return;
        fieldInput.setText(null);
        connection.sendString(fieldNickname.getText() + ": " + msg);
    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        printMessage("Connection ready...");
    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String s) {
        printMessage(s);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        printMessage("Connection close");
    }

    @Override
    public void onException(TCPConnection tcpConnection, IOException e) {
        printMessage("Connection exception: " + e);
    }
    private synchronized void printMessage(String massage) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(massage + "\n");
                log.setCaretPosition(log.getDocument().getLength());
            }
        });
    }
}
