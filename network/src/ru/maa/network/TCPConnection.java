package ru.maa.network;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 *
 *
 * Created by Anton 08.02.2018.
 */

public class TCPConnection {
    private final Socket socket;
    private final Thread thread;
    private final TCPConnectionListener evenListener;
    private final BufferedReader in;
    private final BufferedWriter out;

    public TCPConnection(TCPConnectionListener evenListener, String ipAddress, int port) throws IOException {
        this(evenListener, new Socket(ipAddress, port));
    }

    public TCPConnection(TCPConnectionListener evenListener, Socket socket) throws IOException{
        this.evenListener = evenListener;
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream(),Charset.forName("UTF-8")));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(),Charset.forName("UTF-8")));
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    evenListener.onConnectionReady(TCPConnection.this);
                    while (!thread.isInterrupted()){
                        evenListener.onReceiveString(TCPConnection.this,in.readLine());
                    }
                }catch (IOException e){
                    evenListener.onException(TCPConnection.this, e);
                }finally {
                    evenListener.onDisconnect(TCPConnection.this);
                }
            }
        });
        thread.start();
    }

    public synchronized void sendString(String value){
        try{
            out.write(value + "\r\n");
            out.flush();
        }catch (IOException e) {
            evenListener.onException(this, e);
            disconnect();
        }
    }

    private void disconnect() {

        thread.interrupt();
        try {
            socket.close();
        } catch (IOException e){

            TCPConnectionListener eventListener;
            eventListener = null;
            assert false;
            eventListener.onException(this, e);
        }
    }

    @Override
    public String toString() {
        return "TCPConnection:" + socket.getInetAddress()+ ": "+ socket.getPort();
    }
}