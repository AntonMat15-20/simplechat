package ru.maa.server;

import ru.maa.network.TCPConnectionListener;
import ru.maa.network.TCPConnection;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

/**
 * Anton on 13.02.2018.
 */
public class ChatServer implements TCPConnectionListener {
    public static void main(String[] args) {
        new ChatServer();
    }
    private final ArrayList<TCPConnection> connections = new ArrayList<>();

    private ChatServer() throws RuntimeException {
        System.out.println("Server running...");
        try (ServerSocket serverSocket = new ServerSocket(8189)) {
            while (true) {
                try {
                    new TCPConnection(this, serverSocket.accept());
                } catch (IOException e) {
                    System.out.println("TCPConnection exception: " + e);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException (e);
        }
    }
    @Override
    public void onConnectionReady(ru.maa.network.TCPConnection tcpConnection) {
        connections.add(tcpConnection);
        sendToAllConnections("Client connected: "+ tcpConnection);

    }

    private void sendToAllConnections(String value) {
        System.out.println(value);

        for(TCPConnection connection : connections){
            connection.sendString(value);
        }
    }

    @Override
    public void onReceiveString(ru.maa.network.TCPConnection tcpConnection, String value) {
        sendToAllConnections(value);

    }

    @Override
    public void onDisconnect(ru.maa.network.TCPConnection tcpConnection) {
        connections.remove(tcpConnection);
        sendToAllConnections("Client disconnected: "+tcpConnection);

    }

    @Override
    public void onException(ru.maa.network.TCPConnection tcpConnection, IOException e) {

    }
}